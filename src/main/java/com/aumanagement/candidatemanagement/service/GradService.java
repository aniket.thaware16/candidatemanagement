package com.aumanagement.candidatemanagement.service;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.domain.Trend;
import com.aumanagement.candidatemanagement.repository.GradRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class GradService {
    @Autowired
    GradRepository gradRepository;

    public Grad saveGrad(Grad grad) {
        Long gradId = gradRepository.saveGrad(grad);
        grad.setId(gradId);
        if(grad.getId()!=0L){
            gradRepository.addGradSkills(gradId,grad.getSkills());
        }
        return grad;
    }

    public List<Grad> getAllGrads(){
        return gradRepository.getAllGrads();
    }

    public Grad getGrad(Long id){
        Grad grad =  gradRepository.getGrad(id);
        grad.setSkills(gradRepository.getGradSkills(id));
        return grad;
    }

    public Grad updateGrad(Long id,Grad grad) {
        Long gradNewId = gradRepository.updateGrad(id,grad);
        grad.setId(gradNewId);
        gradRepository.addGradSkills(gradNewId,grad.getSkills());
        return grad;
    }

    public Long deleteGrad(Long id){
        Grad grad = gradRepository.getGrad(id);
        return gradRepository.deleteGrad(id,grad);
    }

    public List<Trend> getTrends(String typeName, int year){
       return gradRepository.getTrends(typeName,year);
    }

    public List<Grad> getGradEditedHistory(Long parentId){
        return gradRepository.getGradEditedHistory(parentId);
    }
}
