package com.aumanagement.candidatemanagement.controller;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.service.GradService;
import com.aumanagement.candidatemanagement.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class GradController {
    @Autowired
    GradService gradService;
    @Autowired
    LoginService loginService;
    @GetMapping("/grad")
    public ResponseEntity getAllGrads(@RequestHeader String token) {
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.getAllGrads(),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @GetMapping("/grad/{id}")
    public ResponseEntity getGrad(@RequestHeader String token,@PathVariable Long id){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.getGrad(id),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @PostMapping("/grad")
    public ResponseEntity saveGrad(@RequestHeader String token,@RequestBody Grad grad){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.saveGrad(grad),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @PutMapping("/grad/{id}")
    public ResponseEntity updateGrad(@RequestHeader String token,@PathVariable Long id,@RequestBody Grad grad){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.updateGrad(id,grad),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("/grad/{id}")
    public ResponseEntity deleteGrad(@RequestHeader String token,@PathVariable Long id){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.deleteGrad(id),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @GetMapping("/trends/{typeName}/{year}")
    public ResponseEntity getTrends(@RequestHeader String token, @PathVariable String typeName,@PathVariable int year){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.getTrends(typeName,year),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @GetMapping("/gradHistory/{id}")
    public ResponseEntity getGradEditedHistory(@RequestHeader String token,@PathVariable Long id){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(gradService.getGradEditedHistory(id),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }
}
