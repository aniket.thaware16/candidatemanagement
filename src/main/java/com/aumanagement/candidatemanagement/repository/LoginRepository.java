package com.aumanagement.candidatemanagement.repository;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LoginRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public int saveUser(User user){
        String insertQuery = "insert into user(user_email,user_name) values('"+user.getUserEmail()+"','"+user.getUserName()+"');";
        String selectQuery = "select * from user where user_email = '"+user.getUserEmail()+"';";
        List checkUser = jdbcTemplate.queryForList(selectQuery);
        if(checkUser.isEmpty()){
            jdbcTemplate.update(insertQuery);
            LoggerConfig.logger.info("user saved"+user.toString());
        }
        return 1;
    }
}
