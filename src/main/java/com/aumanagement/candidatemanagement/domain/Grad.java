package com.aumanagement.candidatemanagement.domain;

import java.sql.Date;
import java.util.List;

public class Grad {
    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String email;
    private String contactNo;
    private String instituteName;
    private String degreeName;
    private String branchName;
    private Date joiningDate;
    private String joiningLocation;
    private String feedback;
    private Date createdOn;
    private String state;
    private String city;
    private String street;
    private String zipCode;
    private String houseNo;
    private String createdByUser;
    private Long parentId;
    private List<Skill> skills;

    public Grad() {
    }

    public Grad(Long id,String firstName,String middleName,String lastName,String gender,String email,String contactNo,String instituteName,String degreeName,String branchName,Date joiningDate,String joiningLocation,String feedback,Date createdOn,String state,String city,String street,String zipCode,String houseNo,String createdByUser,List<Skill> skills,Long parentId) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.contactNo = contactNo;
        this.instituteName = instituteName;
        this.degreeName = degreeName;
        this.branchName = branchName;
        this.joiningDate = joiningDate;
        this.joiningLocation = joiningLocation;
        this.feedback = feedback;
        this.createdOn = createdOn;
        this.state = state;
        this.city = city;
        this.street = street;
        this.zipCode = zipCode;
        this.houseNo = houseNo;
        this.createdByUser = createdByUser;
        this.skills = skills;
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getJoiningLocation() {
        return joiningLocation;
    }

    public void setJoiningLocation(String joiningLocation) {
        this.joiningLocation = joiningLocation;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
