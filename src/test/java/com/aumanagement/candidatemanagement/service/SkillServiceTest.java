package com.aumanagement.candidatemanagement.service;

import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.repository.SkillRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class SkillServiceTest {
    @Mock
    SkillRepository skillRepository;
    @InjectMocks
    SkillService skillService;
    @Test
    void saveSkill() {
        Skill skill = new Skill();
        skill.setSkillName("testSkill");
        Mockito.when(skillRepository.saveSkill(skill)).thenReturn(1);
        int response = skillService.saveSkill(skill);
        assertEquals(1,response);
    }

    @Test
    void getAllSkills() {
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Skill(1L,"Angular","user@gmail.com"));
        skillList.add(new Skill(2L,"Spring","user@gmail.com"));
        skillList.add(new Skill(3L,"Html","user@gmail.com"));
        Mockito.when(skillRepository.getAllSkills()).thenReturn(skillList);
        List<Skill> responseSkill = skillService.getAllSkills();
        assertEquals(skillList.size(),responseSkill.size());
        assertEquals(skillList.get(0).getSkillName(),responseSkill.get(0).getSkillName());
    }
}