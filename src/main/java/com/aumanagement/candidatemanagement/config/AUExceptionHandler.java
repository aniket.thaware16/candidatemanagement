package com.aumanagement.candidatemanagement.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class AUExceptionHandler {

    private static final String NESTED_EXCEPTION_STRING_CONSTANT = " nested exception is";

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleException(RuntimeException re) {
        try {
            if (re instanceof NullPointerException) {
                NullPointerException npe = (NullPointerException) re;
                String npeString = npe.toString();
                LoggerConfig.logger.error(npeString);
                Error error = new Error("internal server problem");
                LoggerConfig.logger.info("nullpointer exception - trying to access the null object");
                return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }


            if (re instanceof ArithmeticException) {
                ArithmeticException ae = (ArithmeticException) re;
                LoggerConfig.logger.error(ae.toString());
                Error error = new Error("Resource Not Found");
                LoggerConfig.logger.info("Encountered WanderResourceNotFoundException - returning NOT_FOUND");
                return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            if (re instanceof ArrayIndexOutOfBoundsException) {
                ArrayIndexOutOfBoundsException ae = (ArrayIndexOutOfBoundsException) re;
                LoggerConfig.logger.error(ae.toString());
                Error error = new Error("ArrayIndexOutOfBoundsException");
                LoggerConfig.logger.info("Encountered ArrayIndexOutOfBoundException - returning FORBIDDEN");
                return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (re instanceof IllegalArgumentException) {
                IllegalArgumentException ie = (IllegalArgumentException) re;
                LoggerConfig.logger.error(ie.toString());
                Error error = new Error("IllegalArgumentException");
                LoggerConfig.logger.info("Encountered IllegalArgumentException - returning FORBIDDEN");
                return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            return resourceExceptionHandler(re);
        }

        catch (Exception e) {
            LoggerConfig.logger.error("Encountered exception while processing runtime exception", e);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public ResponseEntity resourceExceptionHandler(RuntimeException re) {
        LoggerConfig.logger.error("Unexpected RuntimeException encountered", re);
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
