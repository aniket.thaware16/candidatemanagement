package com.aumanagement.candidatemanagement;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CandidatemanagementApplication {
    public static void main(String[] args) {
        SpringApplication.run(CandidatemanagementApplication.class, args);
    }

}
