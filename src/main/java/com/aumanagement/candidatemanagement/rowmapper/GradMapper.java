package com.aumanagement.candidatemanagement.rowmapper;

import com.aumanagement.candidatemanagement.domain.Grad;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class GradMapper implements RowMapper<Grad> {
    @Override
    public Grad mapRow(ResultSet rs, int i) throws SQLException {
        return new Grad(rs.getLong("id"),rs.getString("first_name"),
                rs.getString("middle_name"),rs.getString("last_name"),
                rs.getString("gender"),rs.getString("email"),
                rs.getString("contact_no"),rs.getString("institute_name"),
                rs.getString("degree_name"),rs.getString("branch_name"),
                rs.getDate("joining_date"),rs.getString("joining_location"),
                rs.getString("feedback"),rs.getDate("created_on"),
                rs.getString("state"),rs.getString("city"),rs.getString("street"),
                rs.getString("zip_code"),rs.getString("house_no"),
                rs.getString("created_by_user_email"), null,rs.getLong("parent_id"));
    }
}
