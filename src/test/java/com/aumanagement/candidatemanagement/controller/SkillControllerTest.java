package com.aumanagement.candidatemanagement.controller;

import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.domain.User;
import com.aumanagement.candidatemanagement.service.LoginService;
import com.aumanagement.candidatemanagement.service.SkillService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class SkillControllerTest {
    @Mock
    SkillService skillService;
    @Mock
    LoginService loginService;
    @InjectMocks
    SkillController skillController;


    @Test
    void saveSkill() {
        User user = new User("test@gmail.com","test123");
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        Skill skill = new Skill();
        skill.setSkillName("testSkill");
        Mockito.when(skillService.saveSkill(skill)).thenReturn(1);
        int response = (int) skillController.saveSkill("123456",skill).getBody();
        assertEquals(1,response);
        Error error = (Error) skillController.saveSkill("sampleToken",skill).getBody();
        assertEquals("FAILURE",error.getMessage());
    }

    @Test
    void getAllSkills() {
        User user = new User("test@gmail.com","test123");
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Skill(1L,"Angular","user@gmail.com"));
        skillList.add(new Skill(2L,"Spring","user@gmail.com"));
        skillList.add(new Skill(3L,"Html","user@gmail.com"));
        Mockito.when(skillService.getAllSkills()).thenReturn(skillList);
        List<Skill> responseSkills = (List<Skill>)skillController.getAllSkills("123456").getBody();
        assertEquals(skillList.size(),responseSkills.size());
        Error error = (Error) skillController.getAllSkills("sampleToken").getBody();
        assertEquals("FAILURE",error.getMessage());
    }
}