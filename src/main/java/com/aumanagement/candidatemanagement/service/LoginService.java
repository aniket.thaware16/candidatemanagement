package com.aumanagement.candidatemanagement.service;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.User;
import com.aumanagement.candidatemanagement.repository.LoginRepository;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
public class LoginService {
    @Autowired
    LoginRepository loginRepository;

    public int loginUser(User user){
        return loginRepository.saveUser(user);
    }

    public  boolean validateToken(String tokenString) {
        final String CLIENT_ID = "622766730529-el94n036l9vcpppcqe643ltmg0stdo2t.apps.googleusercontent.com";
        HttpTransport transport = null;
        try {
            transport = GoogleNetHttpTransport.newTrustedTransport();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            LoggerConfig.logger.error(e);
        } catch (IOException e) {
            e.printStackTrace();
            LoggerConfig.logger.error(e);
        }
        JsonFactory jsonFactory = new JacksonFactory();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();

        GoogleIdToken idToken = null;
        try {
            idToken = verifier.verify(tokenString);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            LoggerConfig.logger.error(e);
        } catch (IOException e) {
            e.printStackTrace();
            LoggerConfig.logger.error(e);
        }
        if (idToken != null) {
            return true;
        } else {
            return false;
        }
    }
}
