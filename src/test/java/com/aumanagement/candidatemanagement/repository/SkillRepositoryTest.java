package com.aumanagement.candidatemanagement.repository;

import com.aumanagement.candidatemanagement.domain.Skill;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class SkillRepositoryTest {
    @Autowired
    SkillRepository skillRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;


    @Test
    void saveSkill() {
        String insertUserQuery = "insert into user values('user1@gmail.com','test123')";
        jdbcTemplate.update(insertUserQuery);
        Skill skill= new Skill();
        skill.setSkillName("test");
        skill.setCreatedByUser("user1@gmail.com");
        List<Skill> skillList = skillRepository.getAllSkills();
        skillRepository.saveSkill(skill);
        List<Skill> responseSkillList = skillRepository.getAllSkills();
        assertEquals(skillList.size(),responseSkillList.size()-1);
        skillRepository.saveSkill(skill);
        responseSkillList = skillRepository.getAllSkills();
        assertEquals(skillList.size(),responseSkillList.size()-1);
        String deleteSkillQuery = "delete from skill where id="+responseSkillList.get(responseSkillList.size()-1).getId();
        jdbcTemplate.update(deleteSkillQuery);
        String deleteUserQuery = "delete from user where user_email='user1@gmail.com'";
        jdbcTemplate.update(deleteUserQuery);
    }
}