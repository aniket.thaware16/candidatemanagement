package com.aumanagement.candidatemanagement.repository;

import com.aumanagement.candidatemanagement.domain.User;
import com.aumanagement.candidatemanagement.service.LoginService;
import org.junit.After;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class LoginRepositoryTest {
    @Autowired
    LoginRepository loginRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    void saveUser() {
        User user = new User("test@gmail.com","testFirstName testLastName");
        String selectQuery = "select COUNT(*) as count from user";
        int userCountBeforeAddingUser = jdbcTemplate.queryForObject(selectQuery,Integer.class);
        loginRepository.saveUser(user);
        int userCountafterAddingUser = jdbcTemplate.queryForObject(selectQuery,Integer.class);
        assertTrue(userCountBeforeAddingUser == userCountafterAddingUser-1);
        loginRepository.saveUser(user);
        int userCountafterAddingSameUser = jdbcTemplate.queryForObject(selectQuery,Integer.class);
        assertTrue(userCountafterAddingUser == userCountafterAddingSameUser);
        String deleteQuery = "delete from user where user_email='"+user.getUserEmail()+"'";
        jdbcTemplate.update(deleteQuery);
    }

}