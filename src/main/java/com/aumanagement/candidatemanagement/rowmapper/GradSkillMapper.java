package com.aumanagement.candidatemanagement.rowmapper;

import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.domain.Skill;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GradSkillMapper implements RowMapper<Skill> {
    @Override
    public Skill mapRow(ResultSet rs, int i) throws SQLException {
        return new Skill(rs.getLong("id"),rs.getString("skill_name"),null);
    }
}
