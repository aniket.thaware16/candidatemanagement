package com.aumanagement.candidatemanagement.repository;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.domain.Trend;
import com.aumanagement.candidatemanagement.rowmapper.GradMapper;
import com.aumanagement.candidatemanagement.rowmapper.GradSkillMapper;
import com.aumanagement.candidatemanagement.rowmapper.TrendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

@Repository
public class GradRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public Long saveGrad(Grad grad){
        String insertQuery;
        String selectQuery = "select * from grad where status=1 and email='"+grad.getEmail()+"';";
        List tempGrad = jdbcTemplate.queryForList(selectQuery);
        if(!tempGrad.isEmpty()){
            return 0L;
        }
        insertQuery = "insert into grad(branch_name,city,contact_no,created_on,degree_name,email,feedback,first_name,gender,house_no,institute_name,joining_date,joining_location,last_name, middle_name, state,street,zip_code,created_by_user_email,status) "
                +"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(insertQuery,new String[]{"id"});
            ps.setString(1,grad.getBranchName());
            ps.setString(2,grad.getCity());
            ps.setString(3,grad.getContactNo());
            ps.setString(4,LocalDateTime.now()+"");
            ps.setString(5,grad.getDegreeName());
            ps.setString(6,grad.getEmail());
            ps.setString(7,grad.getFeedback());
            ps.setString(8,grad.getFirstName());
            ps.setString(9,grad.getGender());
            ps.setString(10,grad.getHouseNo());
            ps.setString(11,grad.getInstituteName());
            ps.setString(12,grad.getJoiningDate()+"");
            ps.setString(13,grad.getJoiningLocation());
            ps.setString(14,grad.getLastName());
            ps.setString(15,grad.getMiddleName());
            ps.setString(16,grad.getState());
            ps.setString(17,grad.getStreet());
            ps.setString(18,grad.getZipCode());
            ps.setString(19,grad.getCreatedByUser());
            ps.setInt(20,1);
            return ps;
        },keyHolder);
        String updateQuery = "update grad SET parent_id="+keyHolder.getKey().longValue()+" where id="+keyHolder.getKey().longValue();
        jdbcTemplate.update(updateQuery);
        LoggerConfig.logger.info("Grad saved to db"+keyHolder.getKey().longValue());
        return keyHolder.getKey().longValue();
    }

    public List<Grad> getAllGrads(){
        String selectQuery;
        selectQuery="select * from grad where status=1 order by id DESC";
        return jdbcTemplate.query(selectQuery,new GradMapper());
    }

    public Grad getGrad(Long id){
        String gradQuery = "Select * from  grad where id="+id+";";
        return jdbcTemplate.queryForObject(gradQuery,new GradMapper());
    }

    public List<Skill> getGradSkills(Long id){
        String skillQuery = "Select skill.id,skill.skill_name from skill join grad_skill on skill.id = grad_skill.skill_id where grad_skill.grad_id = "+id+";";
        return jdbcTemplate.query(skillQuery,new GradSkillMapper());
    }

    public Long deleteGrad(Long id,Grad grad) {
        String updateParentQuery = "update grad SET status="+0+" where id="+id;
        jdbcTemplate.update(updateParentQuery);
        String deleteQuery = "insert into grad(branch_name,city,contact_no,created_on,degree_name,email,feedback,first_name,gender,house_no,institute_name,joining_date,joining_location,last_name, middle_name, state,street,zip_code,created_by_user_email,status,parent_id) "
                +"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(deleteQuery,new String[]{"id"});
            ps.setString(1,grad.getBranchName());
            ps.setString(2,grad.getCity());
            ps.setString(3,grad.getContactNo());
            ps.setString(4,LocalDateTime.now()+"");
            ps.setString(5,grad.getDegreeName());
            ps.setString(6,grad.getEmail());
            ps.setString(7,grad.getFeedback());
            ps.setString(8,grad.getFirstName());
            ps.setString(9,grad.getGender());
            ps.setString(10,grad.getHouseNo());
            ps.setString(11,grad.getInstituteName());
            ps.setString(12,grad.getJoiningDate()+"");
            ps.setString(13,grad.getJoiningLocation());
            ps.setString(14,grad.getLastName());
            ps.setString(15,grad.getMiddleName());
            ps.setString(16,grad.getState());
            ps.setString(17,grad.getStreet());
            ps.setString(18,grad.getZipCode());
            ps.setString(19,grad.getCreatedByUser());
            ps.setInt(20,0);
            ps.setLong(21,grad.getParentId());
            return ps;
        },keyHolder);
        LoggerConfig.logger.info("Grad deleted:"+id);
        return keyHolder.getKey().longValue();
    }

    public Long updateGrad(Long id,Grad grad){
        String updateParentQuery = "update grad SET status="+0+" where id="+id;
        LoggerConfig.logger.info("Grad status changed:"+id);
        jdbcTemplate.update(updateParentQuery);
        String insertQuery = "insert into grad(branch_name,city,contact_no,created_on,degree_name,email,feedback,first_name,gender,house_no,institute_name,joining_date,joining_location,last_name, middle_name, state,street,zip_code,created_by_user_email,status,parent_id) "
                +"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(insertQuery,new String[]{"id"});
            ps.setString(1,grad.getBranchName());
            ps.setString(2,grad.getCity());
            ps.setString(3,grad.getContactNo());
            ps.setString(4,LocalDateTime.now()+"");
            ps.setString(5,grad.getDegreeName());
            ps.setString(6,grad.getEmail());
            ps.setString(7,grad.getFeedback());
            ps.setString(8,grad.getFirstName());
            ps.setString(9,grad.getGender());
            ps.setString(10,grad.getHouseNo());
            ps.setString(11,grad.getInstituteName());
            ps.setString(12,grad.getJoiningDate()+"");
            ps.setString(13,grad.getJoiningLocation());
            ps.setString(14,grad.getLastName());
            ps.setString(15,grad.getMiddleName());
            ps.setString(16,grad.getState());
            ps.setString(17,grad.getStreet());
            ps.setString(18,grad.getZipCode());
            ps.setString(19,grad.getCreatedByUser());
            ps.setInt(20,1);
            ps.setLong(21,grad.getParentId());
            return ps;
        },keyHolder);
        LoggerConfig.logger.info("Grad updated:newId:"+keyHolder.getKey().longValue());
        return keyHolder.getKey().longValue();
    }

    public int addGradSkills(Long id,List<Skill> skills){
        if(skills.size()==0){
            LoggerConfig.logger.info("No skills of Id to be added:"+id);
            return 0;
        }
        String insertQuery= "insert into grad_skill values";
        ListIterator<Skill> skillIterator = skills.listIterator();
        while (skillIterator.hasNext()) {
            insertQuery += "("+id + "," + skillIterator.next().getId() + "),";
        }
        LoggerConfig.logger.info("Skills added of grad:"+id);
        return jdbcTemplate.update(insertQuery.substring(0,insertQuery.length()-1));
    }


    public List<Trend> getTrends(String typeName,int year){
        String selectQuery = "";
        switch (typeName){
            case "Location": selectQuery +=  "select joining_location as name,COUNT(*) as count from grad where status=1 and extract(YEAR from joining_date)="+year+
                    " group by  joining_location";
                    break;
            case "Skills": selectQuery += "select skill.skill_name as name,COUNT(*) as count from grad\n" +
                    "join grad_skill on grad.id = grad_skill.grad_id\n" +
                    "join skill on grad_skill.skill_id = skill.id\n" +
                    "where grad.status=1 and extract(YEAR from grad.joining_date)="+year+
                    " group by  skill.id";
        }
        LoggerConfig.logger.info(typeName+","+year+":Trends Data");
       return jdbcTemplate.query(selectQuery,new TrendMapper());
    }

    public List<Grad> getGradEditedHistory(Long parentId){
        String selectQuery;
        selectQuery="select * from grad  where parent_id="+parentId+" order by id DESC";
        return jdbcTemplate.query(selectQuery,new GradMapper());
    }
}
