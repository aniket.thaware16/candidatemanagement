package com.aumanagement.candidatemanagement.service;

import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillService {
    @Autowired
    SkillRepository skillRepository;

    public int saveSkill(Skill skill){
        return skillRepository.saveSkill(skill);
    }

    public List<Skill> getAllSkills(){
        return skillRepository.getAllSkills();
    }
}
