package com.aumanagement.candidatemanagement.controller;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.User;
import com.aumanagement.candidatemanagement.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class LoginController {
    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestHeader String token,@RequestBody User user) throws GeneralSecurityException, IOException {
        if(loginService.validateToken(token)) {
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(loginService.loginUser(user), HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
         return new ResponseEntity(new Error("FAILURE"), HttpStatus.OK);
    }
}
