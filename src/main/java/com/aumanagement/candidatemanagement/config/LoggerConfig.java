package com.aumanagement.candidatemanagement.config;

import com.aumanagement.candidatemanagement.CandidatemanagementApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerConfig {
    public static final Logger logger = LogManager.getLogger(CandidatemanagementApplication.class.getName());
}
