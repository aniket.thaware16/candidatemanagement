package com.aumanagement.candidatemanagement.controller;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.service.LoginService;
import com.aumanagement.candidatemanagement.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class SkillController {
    @Autowired
    SkillService skillService;
    @Autowired
    LoginService loginService;
    @PostMapping("/skill")
    public ResponseEntity saveSkill(@RequestHeader String token, @RequestBody Skill skill){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(skillService.saveSkill(skill), HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }

    @GetMapping("/skill")
    public ResponseEntity getAllSkills(@RequestHeader String token){
        if(loginService.validateToken(token)){
            LoggerConfig.logger.info("Token Valid");
            return new ResponseEntity(skillService.getAllSkills(),HttpStatus.OK);
        }
        LoggerConfig.logger.error("Google token NOT VALID:"+token);
        return new ResponseEntity(new Error("FAILURE"),HttpStatus.FORBIDDEN);
    }
}
