package com.aumanagement.candidatemanagement.repository;

import com.aumanagement.candidatemanagement.config.LoggerConfig;
import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.rowmapper.SkillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class SkillRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    public int saveSkill(Skill skill){
        String selectQuery = "select * from skill where skill_name='"+skill.getSkillName()+"';";
        List tempSkills = jdbcTemplate.queryForList(selectQuery);
        if(!tempSkills.isEmpty()){
            return 0;
        }
        String insertQuery = "insert into skill(created_on,skill_name,created_by_user_email) values('"+
                LocalDateTime.now() +"','"+skill.getSkillName()+"','"+skill.getCreatedByUser()+"');";
        LoggerConfig.logger.info("Skill adding"+skill.toString());
        return jdbcTemplate.update(insertQuery);
    }

    public List<Skill> getAllSkills() {
        String selectQuery= "select * from skill";
        return jdbcTemplate.query(selectQuery,new SkillMapper());
    }
}
