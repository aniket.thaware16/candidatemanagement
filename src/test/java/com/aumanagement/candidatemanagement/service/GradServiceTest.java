package com.aumanagement.candidatemanagement.service;

import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.domain.Trend;
import com.aumanagement.candidatemanagement.repository.GradRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class GradServiceTest {
    @Mock
    GradRepository gradRepository;
    @InjectMocks
    GradService gradService;

    @Test
    void saveGrad() {
        Grad grad = new Grad();
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Skill(1L,"Angular","user@gmail.com"));
        skillList.add(new Skill(2L,"Spring","user@gmail.com"));
        skillList.add(new Skill(3L,"Html","user@gmail.com"));
        grad.setEmail("test@gmail.com");
        grad.setSkills(skillList);
        Mockito.when(gradRepository.saveGrad(grad)).thenReturn(1L);
        Mockito.when(gradRepository.addGradSkills(1L,skillList)).thenReturn(3);
        Grad responseGrad = gradService.saveGrad(grad);
        assertEquals(grad.getEmail(),responseGrad.getEmail());
        assertEquals(grad.getSkills().size(),responseGrad.getSkills().size());
    }

    @Test
    void getAllGrads() {
        List<Grad> gradList = new ArrayList<>();
        for(int i=0;i<5;i++){
            Grad grad  = new Grad();
            grad.setEmail("test"+i+"@gmail.com");
            gradList.add(grad);
        }
        Mockito.when(gradRepository.getAllGrads()).thenReturn(gradList);
        List<Grad>responseGradList = gradService.getAllGrads();
        assertEquals(gradList.size(),responseGradList.size());
    }

    @Test
    void getGrad() {
        Grad grad = new Grad();
        grad.setEmail("test@gmail.com");
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Skill(1L,"Angular","user@gmail.com"));
        skillList.add(new Skill(2L,"Spring","user@gmail.com"));
        skillList.add(new Skill(3L,"Html","user@gmail.com"));
        grad.setSkills(skillList);
        Mockito.when(gradRepository.getGrad(1L)).thenReturn(grad);
        Mockito.when(gradRepository.getGradSkills(1L)).thenReturn(skillList);
        Grad responseGrad = gradService.getGrad(1L);
        assertEquals(grad.getSkills().size(),responseGrad.getSkills().size());
    }

    @Test
    void updateGrad() {
        Grad grad = new Grad();
        grad.setEmail("test@gmail.com");
        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Skill(1L,"Angular","user@gmail.com"));
        skillList.add(new Skill(2L,"Spring","user@gmail.com"));
        skillList.add(new Skill(3L,"Html","user@gmail.com"));
        grad.setSkills(skillList);
        Mockito.when(gradRepository.updateGrad(1L,grad)).thenReturn(2L);
        Mockito.when(gradRepository.addGradSkills(2L,skillList)).thenReturn(3);
        Grad responseGrad = gradService.updateGrad(1L,grad);
        assertEquals(grad.getEmail(),responseGrad.getEmail());
        assertEquals(grad.getSkills().size(),responseGrad.getSkills().size());
    }

    @Test
    void deleteGrad() {
        Grad grad = new Grad();
        grad.setEmail("test@gmail.com");
        Mockito.when(gradRepository.getGrad(1L)).thenReturn(grad);
        Mockito.when(gradRepository.deleteGrad(1L,grad)).thenReturn(2L);
        Long responseGradId = gradService.deleteGrad(1L);
        assertEquals(2L,responseGradId);
    }

    @Test
    void getTrends() {
        List<Trend> trends = new ArrayList<>();
        trends.add(new Trend("test1",2));
        trends.add(new Trend("test2",5));
        trends.add(new Trend("test3",10));
        Mockito.when(gradRepository.getTrends("Location",2020)).thenReturn(trends);
        List<Trend> responseTrends = gradService.getTrends("Location",2020);
        assertEquals(trends.get(0).getCount(),responseTrends.get(0).getCount());
        assertEquals(trends.size(),responseTrends.size());
    }

    @Test
    void getGradEditedHistory(){
        List<Grad> gradList = new ArrayList<>();
        gradList.add(new Grad());
        gradList.add(new Grad());
        gradList.get(0).setId(1L);
        gradList.get(0).setParentId(1L);
        gradList.get(1).setId(2L);
        gradList.get(1).setParentId(1L);
        Mockito.when(gradRepository.getGradEditedHistory(1L)).thenReturn(gradList);
        List<Grad> responseGradList = gradService.getGradEditedHistory(1L);
        assertEquals(gradList.size(),responseGradList.size());
        assertEquals(1L,responseGradList.get(0).getParentId());
    }
}