package com.aumanagement.candidatemanagement.service;

import com.aumanagement.candidatemanagement.domain.User;
import com.aumanagement.candidatemanagement.repository.LoginRepository;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class LoginServiceTest {
    @Mock
    LoginRepository loginRepository;
    @InjectMocks
    LoginService loginService;

    @Test
    public void loginUser() {
        User user = new User("test@gmail.com","testFirstName testLastName");
        Mockito.when(loginRepository.saveUser(user)).thenReturn(1);
        int response  = loginService.loginUser(user);
        assertEquals(1,response);
    }

    @Test
    void validateToken() {
        String token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ5MjcxMGE3ZmNkYjE1Mzk2MGNlMDFmNzYwNTIwYTMyYzg0NTVkZmYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjIyNzY2NzMwNTI5LWVsOTRuMDM2bDl2Y3BwcGNxZTY0M2x0bWcwc3RkbzJ0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjyNzY2NzMwNTI5LWVsOTRuMDM2bDl2Y3BwcGNxZTY0M2x0bWcwc3RkbzJ0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE2NjU2NTI1OTMxOTM3NzI5NTgxIiwiaGQiOiJhY2NvbGl0ZWluZGlhLmNvbSIsImVtYWlsIjoiYW5pa2V0cmFqZW5kcmEudGhhd2FyZUBhY2NvbGl0ZWluZGlhLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiQ2tESVZfOWUxdEdrQWlFcmJBZEN4USIsIm5hbWUiOiJBbmlrZXQgUmFqZW5kcmEgVGhhd2FyZSIsInBpY3R1cmUiOiJodHRwczovL2xoNS5nb29nbGV1c2VyY29udGVudC5jb20vLTBOdmJZY3hsdFVFL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FNWnV1Y25FT2xtMHdCbjlxSXdnLXRSWE1zS0EwWElzUXcvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IkFuaWtldCBSYWplbmRyYSIsImZhbWlseV9uYW1lIjoiVGhhd2FyZSIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTkxNjI5ODM3LCJleHAiOjE1OTE2MzM0MzcsImp0aSI6ImIxMWY0Nzc0ZDk1MmE5ZTExZGQ5Y2UzYzU4NmZjOTFlMzJjMWU1NTAifQ.SAauFiClZZqVcF7gX1oB9mA4d3e876AP2oxiKO0Kg17bQMOe6Gcgzy9C0A8RVIvU_YFA2iJwVGj_hlojViPqE3eQfXM8fmducbpbGSAYLq_kubSeUnDfofkz5kDLesvtAf5fVpyiXoErmHnXRszHoqcH-pqylW063q-rlErXnAOLWIgoAZyqy1DLyTUu00iWUvvo967bjf3Xztrs-1Ted0pbJlhggHsTs4Oi8glqKAbogcuOOkZ0pcUHhww_-nv0d8aUwVvbPgai4fbSZkPQm8XHKRuX1bEGOs4YF1HaBPIEXQVRUpOdgngRH0bDZ-jQC9Wfnr0oaMXOXEtGdcr9mw";
        boolean response = loginService.validateToken(token);
        assertEquals(false,response);
    }
}