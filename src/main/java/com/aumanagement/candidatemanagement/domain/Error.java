package com.aumanagement.candidatemanagement.domain;

public class Error {
    private String errorMsg;

    public Error(){
    }
    public Error(String errorMsg){
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
