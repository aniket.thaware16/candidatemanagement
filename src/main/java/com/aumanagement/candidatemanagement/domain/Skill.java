package com.aumanagement.candidatemanagement.domain;

import java.sql.Date;
import java.util.List;


public class Skill {
    private Long id;
    private String skillName;
    private String createdByUser;

    public Skill(){
    }

    public Skill(Long id,String skillName,String createdByUser){
        this.id = id;
        this.skillName = skillName;
        this.createdByUser = createdByUser;
    }

    public Long getId() {
        return id;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

}
