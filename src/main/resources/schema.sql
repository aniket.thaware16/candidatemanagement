
CREATE TABLE IF NOT EXISTS candidate_management.user (
  user_email varchar(255) NOT NULL,
  user_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (user_email)
);

CREATE TABLE IF NOT EXISTS candidate_management.grad (
  id bigint(20) NOT NULL auto_increment,
  email varchar(255) NOT NULL,
  contact_no varchar(13) DEFAULT NULL,
  first_name varchar(50) NOT NULL,
  middle_name varchar(50) NOT NULL,
  last_name varchar(50) NOT NULL,
  gender varchar(10) NOT NULL,
  institute_name varchar(255) DEFAULT NULL,
  branch_name varchar(50) DEFAULT NULL,
  degree_name varchar(50) DEFAULT NULL,
  state varchar(50) DEFAULT NULL,
  city varchar(20) DEFAULT NULL,
  street varchar(100) DEFAULT NULL,
  zip_code varchar(10) DEFAULT NULL,
  house_no varchar(10) DEFAULT NULL,
  joining_date date DEFAULT NULL,
  joining_location varchar(255) DEFAULT NULL,
  feedback text DEFAULT NULL,
  parent_id bigint(20) DEFAULT NULL,
  status int DEFAULT 1,
  created_on datetime NOT NULL,
  created_by_user_email varchar(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (created_by_user_email) REFERENCES candidate_management.user(user_email)
);

CREATE TABLE IF NOT EXISTS candidate_management.skill (
  id bigint(20) NOT NULL auto_increment,
  skill_name varchar(255) NOT NULL,
  created_on datetime NOT NULL,
  created_by_user_email varchar(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (created_by_user_email) REFERENCES candidate_management.user(user_email)
);

CREATE TABLE IF NOT EXISTS candidate_management.grad_skill (
  grad_id bigint(20) NOT NULL,
  skill_id bigint(20) NOT NULL,
  PRIMARY KEY (grad_id,skill_id),
  FOREIGN KEY (grad_id) REFERENCES candidate_management.grad(id),
  FOREIGN KEY (skill_id) REFERENCES candidate_management.skill(id)
);



