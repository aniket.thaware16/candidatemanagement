package com.aumanagement.candidatemanagement.controller;

import com.aumanagement.candidatemanagement.domain.User;
import com.aumanagement.candidatemanagement.service.LoginService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class LoginControllerTest {
    @Mock
    LoginService loginService;
    @InjectMocks
    LoginController loginController;
    @Test
    void login() throws GeneralSecurityException, IOException {
        User user = new User("test@gmail.com","test123");
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        Mockito.when(loginService.loginUser(user)).thenReturn(1);
        int response = (int) loginController.login("123456",user).getBody();
        assertEquals(1,response);
        Error error = (Error) loginController.login("sampleToken",user).getBody();
        assertEquals("FAILURE",error.getMessage());
    }
}