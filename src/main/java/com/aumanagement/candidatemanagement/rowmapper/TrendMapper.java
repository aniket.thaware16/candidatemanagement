package com.aumanagement.candidatemanagement.rowmapper;

import com.aumanagement.candidatemanagement.domain.Trend;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrendMapper implements RowMapper<Trend> {
    @Override
    public Trend mapRow(ResultSet rs, int i) throws SQLException {
        return new Trend(rs.getString("name"),rs.getInt("count"));
    }
}
