package com.aumanagement.candidatemanagement.repository;

import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.domain.Trend;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class GradRepositoryTest {
    @Autowired
    GradRepository gradRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    SkillRepository skillRepository;

    @BeforeEach
    void init(){
        String insertUserQuery = "insert into user values('user@gmail.com','test123')";
        jdbcTemplate.update(insertUserQuery);
    }
    @Test
    void saveGrad() {
        List<Skill> skills = new ArrayList<>();
        Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com","1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
        Long graId = gradRepository.saveGrad(grad);
        Grad testGrad = gradRepository.getGrad(graId);
        assertEquals(grad.getEmail(),testGrad.getEmail());
        assertEquals(grad.getContactNo(),testGrad.getContactNo());
        assertEquals(graId,testGrad.getId());
        Long newGradId = gradRepository.saveGrad(grad);
        assertEquals(0L,newGradId);
        deleteGradFromDb(graId);
    }

    @Test
    void getAllGrads() {
        List<Skill> skills = new ArrayList<>();
        List<Grad> gradList = new ArrayList<>();
        List<Grad> beforeGradList = gradRepository.getAllGrads();
        for(int i=0;i<5;i++){
            Grad grad = new Grad(0L,"testFirstName"+i,"testMiddleName"+i,"testLastName"+i,"Male","testgrad@gmail.com"+i,"1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
            grad.setId(gradRepository.saveGrad(grad));
            gradList.add(grad);
        }
        List<Grad> responseGradList = gradRepository.getAllGrads();
        assertEquals(beforeGradList.size(),responseGradList.size()-5);
        for(int i=0;i<5;i++){
            deleteGradFromDb(gradList.get(i).getId());
        }
    }

    @Test
    void getGrad() {
        List<Skill> skills = new ArrayList<>();
        Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com","1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
        grad.setId(gradRepository.saveGrad(grad));
        Grad responseGrad = gradRepository.getGrad(grad.getId());
        assertEquals(grad.getId(),responseGrad.getId());
        assertEquals(grad.getEmail(),responseGrad.getEmail());
        deleteGradFromDb(grad.getId());
    }

    @Test
    void getGradSkills() {
        List<Skill> skills = skillRepository.getAllSkills();
        Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com","1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
        grad.setId(gradRepository.saveGrad(grad));
        gradRepository.addGradSkills(grad.getId(),grad.getSkills());
        List<Skill> responseGradSkills = gradRepository.getGradSkills(grad.getId());
        assertEquals(skills.size(),responseGradSkills.size());
        deleteGradSkills(grad.getId());
        deleteGradFromDb(grad.getId());
    }

    @Test
    void deleteGrad() {
        List<Skill> skills = new ArrayList<>();
        Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com","1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
        grad.setId(gradRepository.saveGrad(grad));
        Long responseGradId = gradRepository.deleteGrad(grad.getId(),grad);
        String selectQuery = "select status from grad where id="+responseGradId;
        int status = jdbcTemplate.queryForObject(selectQuery,Integer.class);
        assertEquals(0,status);
        deleteGradFromDb(grad.getId());
        deleteGradFromDb(responseGradId);
    }

    @Test
    void updateGrad() {
        List<Skill> skills = new ArrayList<>();
        Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com","1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
        grad.setId(gradRepository.saveGrad(grad));
        grad.setCity("testupdateCity");
        Long responseGradId = gradRepository.updateGrad(grad.getId(),grad);
        assertEquals(grad.getCity(),gradRepository.getGrad(responseGradId).getCity());
        deleteGradFromDb(grad.getId());
        deleteGradFromDb(responseGradId);
    }

    @Test
    void addGradSkills() {
        List<Skill> skills = new ArrayList<>();
        int response = gradRepository.addGradSkills(1L,skills);
        assertEquals(0,response);
    }

    @Test
    void getTrends() {
        List<Skill> skills = new ArrayList<>();
        skills.add(new Skill(0L,"test1Skill","user@gmail.com"));
        skills.add(new Skill(0L,"test2Skill","user@gmail.com"));
        skillRepository.saveSkill(skills.get(0));
        skillRepository.saveSkill(skills.get(1));
        List<Skill> allSkills = skillRepository.getAllSkills();
        skills = allSkills.subList(allSkills.size()-2,allSkills.size());
        List<Grad> gradList = new ArrayList<>();
        for(int i=0;i<2;i++){
            Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com"+i,"1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"testLocation"+i,"testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
            grad.setId(gradRepository.saveGrad(grad));
            gradRepository.addGradSkills(grad.getId(),grad.getSkills());
            gradList.add(grad);
        }
        List<Trend> skillsTrends = gradRepository.getTrends("Skills",2020);
        List<Trend> locationTrends = gradRepository.getTrends("Location",2020);
        assertEquals(2,skillsTrends.get(skillsTrends.size()-1).getCount());
        assertEquals(1,locationTrends.get(locationTrends.size()-1).getCount());
        for(int i=0;i<2;i++){
            deleteGradSkills(gradList.get(i).getId());
            deleteGradFromDb(gradList.get(i).getId());
        }
        deleteSkill(skills.get(0).getSkillName());
        deleteSkill(skills.get(1).getSkillName());
    }

    @Test
    void getGradEditedHistory(){
        List<Skill> skills = new ArrayList<>();
        Grad grad = new Grad(0L,"testFirstName","testMiddleName","testLastName","Male","testgrad@gmail.com","1234567890","testInstitutename","testDegreeName","testBranchName", Date.valueOf("2020-05-05"),"Mumbai","testFeedback",null,"testState","testCity","test Street","123456","28","user@gmail.com",skills,0L);
        grad.setId(gradRepository.saveGrad(grad));
        grad.setParentId(grad.getId());
        grad.setCity("testupdateCity");
        Long updatedGradId = gradRepository.updateGrad(grad.getId(),grad);
        Grad updatedGrad = gradRepository.getGrad(updatedGradId);
        List<Grad> responseGradList = gradRepository.getGradEditedHistory(updatedGrad.getParentId());
        assertEquals(2,responseGradList.size());
        deleteGradFromDb(grad.getId());
        deleteGradFromDb(updatedGradId);
    }

    @AfterEach
    void tearDown() {
        String deleteUserQuery = "delete from user where user_email='user@gmail.com'";
        jdbcTemplate.update(deleteUserQuery);
    }


    public void deleteGradFromDb(Long id){
        String deleteQuery = "delete from grad where id="+id;
        jdbcTemplate.update(deleteQuery);
    }

    public void deleteGradSkills(Long id){
        String deleteSkillsQuery = "delete from grad_skill where grad_id="+id;
        jdbcTemplate.update(deleteSkillsQuery);
    }

    public void deleteSkill(String skillName){
        String deleteQuery = "delete from skill where skill_name='"+skillName+"';";
        jdbcTemplate.update(deleteQuery);
    }

}