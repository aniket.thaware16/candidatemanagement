package com.aumanagement.candidatemanagement.controller;

import com.aumanagement.candidatemanagement.domain.Grad;
import com.aumanagement.candidatemanagement.domain.Skill;
import com.aumanagement.candidatemanagement.domain.Trend;
import com.aumanagement.candidatemanagement.service.GradService;
import com.aumanagement.candidatemanagement.service.LoginService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;


import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
class GradControllerTest {
    @Mock
    GradService gradService;
    @Mock
    LoginService loginService;
    @InjectMocks
    GradController gradController;

    @Test
    void getAllGrads() {
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        List<Grad> gradList = new ArrayList<>();
        for(int i=0;i<10;i++){
            Grad testGrad = new Grad();
            testGrad.setEmail("grad"+i+"@gmail.com");
            gradList.add(testGrad);
        }
        Mockito.when(gradService.getAllGrads()).thenReturn(gradList);
        List<Grad> responsegradList = (List<Grad>) gradController.getAllGrads("123456").getBody();
        assertEquals(10,responsegradList.size());
        assertEquals(gradList.get(0).getEmail(),responsegradList.get(0).getEmail());
        Error error = (Error)gradController.getAllGrads("sampleToken").getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.getAllGrads("sampleToken").getStatusCode());
    }
    @SuppressWarnings("unchecked")
    @Test
    void getGrad() throws GeneralSecurityException, IOException {
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        Grad grad = new Grad();
        grad.setEmail("testUser@gmail.com");
        Mockito.when(gradService.getGrad(1L)).thenReturn(grad);
        Grad newGrad = (Grad)gradController.getGrad("123456",1L).getBody();
        assertEquals(grad.getEmail(),newGrad.getEmail());
        Error error = (Error)gradController.getGrad("sampleToken",1L).getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.getGrad("sampleToken",1L).getStatusCode());
    }

    @Test
    void saveGrad() {
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        Grad grad = new Grad();
        grad.setEmail("testUser@gmail.com");
        Mockito.when(gradService.saveGrad(grad)).thenReturn(grad);
        Grad newGrad = (Grad)gradController.saveGrad("123456",grad).getBody();
        assertEquals(grad.getEmail(),newGrad.getEmail());
        Error error = (Error)gradController.saveGrad("sampleToken",grad).getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.getGrad("sampleToken",1L).getStatusCode());
    }

    @Test
    void updateGrad() {
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        Grad grad = new Grad();
        grad.setEmail("testUser@gmail.com");
        Mockito.when(gradService.updateGrad(1L,grad)).thenReturn(grad);
        Grad responseGrad = (Grad)gradController.updateGrad("123456",1L,grad).getBody();
        assertEquals(grad.getEmail(),responseGrad.getEmail());
        Error error = (Error)gradController.updateGrad("sampleToken",1L,grad).getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.updateGrad("sampleToken",1L,grad).getStatusCode());
    }

    @Test
    void deleteGrad() {
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        Mockito.when(gradService.deleteGrad(1L)).thenReturn(1L);
        assertEquals(HttpStatus.OK,gradController.deleteGrad("123456",1L).getStatusCode());
        Error error = (Error)gradController.deleteGrad("sampleToken",1L).getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.deleteGrad("sampleToken",1L).getStatusCode());
    }

    @Test
    void getTrends() {
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        List<Trend> trendsList = new ArrayList<>();
        trendsList.add(new Trend("Mumbai",20));
        trendsList.add(new Trend("Delhi",12));
        trendsList.add(new Trend("Hyderabad",22));
        Mockito.when(gradService.getTrends("Location",2020)).thenReturn(trendsList);
        List<Trend> responseTrends = (List<Trend>) gradController.getTrends("123456","Location",2020).getBody();
        assertEquals(trendsList.get(0).getCount(), responseTrends.get(0).getCount());
        Error error = (Error)gradController.getTrends("sampleToken","Location",2020).getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.getTrends("sampleToken","Location",2020).getStatusCode());
    }

    @Test
    void getGradEditedHistory(){
        Mockito.when(loginService.validateToken("123456")).thenReturn(true);
        List<Grad> gradList = new ArrayList<>();
        gradList.add(new Grad());
        gradList.add(new Grad());
        gradList.get(0).setId(1L);
        gradList.get(0).setParentId(1L);
        gradList.get(1).setId(2L);
        gradList.get(1).setParentId(1L);
        Mockito.when(gradService.getGradEditedHistory(1L)).thenReturn(gradList);
        List<Grad> responseGradList = (List<Grad>) gradController.getGradEditedHistory("123456",1L).getBody();
        assertEquals(2,responseGradList.size());
        Error error = (Error)gradController.getGradEditedHistory("sampleToken",1L).getBody();
        assertEquals("FAILURE",error.getMessage());
        assertEquals(HttpStatus.FORBIDDEN,gradController.getGradEditedHistory("sampleToken",1L).getStatusCode());
    }
}